# Import restify
restify = require 'restify'
request = require 'request'

# Handle route functions
incoming = (req, res, next) ->
  console.log 'incoming received'
  # console.log req.params
  console.log 'recipient, sender, from, subject, body-plain, body-html, message-headers'
  console.log req.params.recipient
  console.log req.params.sender
  console.log req.params.from
  console.log req.params.subject
  console.log req.params['body-plain']
  console.log req.params['body-html']
  console.log req.params['message-headers']
  # The message headers are passed as a type string, in JSON
  incomingHeaders = JSON.parse req.params['message-headers']

  # Construct an object (with optional arrays) for the headers
  headers = {}
  for pieces in incomingHeaders
    unless headers[pieces[0]]?
      headers[pieces[0]] = []
    headers[pieces[0]].push pieces[1]
  for key, val of headers
    if val.length is 1
      headers[key] = val[0]
  # console.log 'headers', headers

  # Let's try to forward this message now...
  apiReq =
    uri: 'https://api.mailgun.net/v3/' + process.env.DOMAIN + '/messages'
    method: 'POST'
    auth:
      user: 'api'
      pass: process.env.APIKEY
    form:
      from: req.params.from
      to: process.env.TO
      subject: req.params.subject
      text: req.params['body-plain']
      html: req.params['body-html']
    qsStringifyOptions:
      arrayFormat:
        'repeat'
  # Maybe we don't need the headers, the Received: headers appear to be stripped
  # anyway, and the rest are probably not meant to be forwarded... :-(
  for header, value of headers
    console.log 'adding header', header, value
    apiReq.form['h' + header] = value
  console.log 'req.form', apiReq.form

  request apiReq, (error, resp, body) ->
    console.log 'mailgun request callback'
    # console.log error, resp, body
    console.log error, body


  res.send 'hello'
  next()

# Define routes
server = restify.createServer()
server.use restify.bodyParser()
server.post '/incoming', incoming

# Boot the server
server.listen 8080, ->
  console.log '%s listening at %s', server.name, server.url